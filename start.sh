#!/bin/sh

install=(
"git"
"htop"
"wget"
"curl"
"nmap"
"geoip"
"jenv"
"node"
"yarn"
"maven"
"gradle"
"openssl"
"python3"
"docker-compose"
"telnet"
"kubernetes-cli"
"awscli"
)

cask=(
"iterm2"
"adobe-acrobat-reader"
"docker"
"firefox"
"intellij-idea"
"keepassx"
"mamp"
"google-chrome"
"adoptopenjdk8"
"adoptopenjdk12"
"messenger"
"slack"
"whatsapp"
"discord"
"sublime-text"
"visual-studio-code"
"spotify"
"vlc"
"keystore-explorer"
"font-hack-nerd-font"
)

if [[ $(command -v brew) == "" ]]; then 
	echo -e "\x1B[96mInstalling Homebrew.. \x1B[39m"
	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
	echo -e "\x1B[96mUpdating Homebrew.. \x1B[39m"
	brew update
fi

echo -e "\x1B[96mCheck for brew cask.. \x1B[39m"
brew tap caskroom/cask
brew tap homebrew/cask-fonts

for i in ${install[@]}
do
	if brew ls --versions $i > /dev/null; then
		echo -e "\x1B[95m--- Upgrading $i.. \x1B[39m"
		brew upgrade $i
	else
		echo -e "\x1B[93m--- Installing $i.. \x1B[39m"
		brew install $i
	fi
done

for j in ${cask[@]}
do
	if brew ls --versions $j > /dev/null; then
		echo -e "\x1B[95m--- Upgrading $j.. \x1B[39m"
		brew cask upgrade $j
	else
		echo -e "\x1B[93m--- Installing $j.. \x1B[39m"
		brew cask install $j
	fi
done

echo -e "\x1B[93m--- Installing @angular/cli.. \x1B[39m"
npm install -g @angular/cli

echo -e "\x1B[96mBrew cleanup.. \x1B[39m"
brew cleanup

echo -e "\x1B[93m--- Installing Oh My Zsh.. \x1B[39m"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

echo -e "\x1B[93m--- Installing powerlevel9k Zsh theme.. \x1B[39m"
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
